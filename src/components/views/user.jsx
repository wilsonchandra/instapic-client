import React, { Component } from "react";

export default class User extends Component {
    constructor(props) {
      super(props);
      this.state = {
          value: '',
          error: null,
          isLoaded: false,
          posts: []
        };
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      event.preventDefault();

      fetch("http://" + window.location.hostname + ":5000/user/" + this.state.value)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            posts: result,
            error: null
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )

    }
  
    render() {
      const { error, isLoaded, posts } = this.state;
      return (
        <div id="feed">
            <form onSubmit={this.handleSubmit}>
            <label>
                Username:
                <input type="text" value={this.state.value} onChange={this.handleChange} />
            </label>
            <input type="submit" value="Submit" />
            </form>

            { this.state.isLoaded &&
                <div>
                  <ul>
                    {posts.map(post => (
                      <li key={post.id}>
                        {post.username} {post.description} 
                        <br></br>
                        <img src= {"http://" + window.location.hostname + ":5000/files/" + post.filename} />
                        <br></br><br></br>
                      </li>
                    ))}
                  </ul>
                </div>
            }
        </div>
      );
    }
  }