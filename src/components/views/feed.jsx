import React, { Component } from "react";

export default class Feed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      posts: []
    };
  }

  componentDidMount() {
    fetch("http://" + window.location.hostname + ":5000/feed")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            posts: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, posts } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div id="feed">
          <ul>
            {posts.map(post => (
              <li key={post.id}>
                {post.username} {post.description} 
                <br></br>
                <img src= {"http://" + window.location.hostname + ":5000/files/" + post.filename} />
                <br></br><br></br>
              </li>
            ))}
          </ul>
        </div>
      );
    }
  }
}