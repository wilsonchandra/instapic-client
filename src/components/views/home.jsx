import React, { Component } from "react";
import { browserHistory } from 'react-router';

export default class Home extends Component {
  componentDidMount() {
    browserHistory.push('/');
  }
  render() {
    return (
      <div id="home">
        This is the home page.
        <br></br><br></br>
        <a href="feed">View Feed</a>
        <br></br><br></br>
        <a href="user">Find images by User</a>
        <br></br><br></br>
        <a href="upload">Upload Images</a>
      </div>
    );
  }
}