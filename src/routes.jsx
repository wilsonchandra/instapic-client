import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import Home from './components/views/home';
import Feed from './components/views/feed';
import User from './components/views/user';
import Upload from './components/views/upload';

export default (
  <Route path='/' component={App}>
    <IndexRoute component={Home} />
    <Route path='feed' component={Feed} />
    <Route path='user' component={User} />
    <Route path='upload' component={Upload} />
    <Route path='*' component={Home} />
  </Route>
);