import React, { Component } from "react";

export default class Upload extends Component {
    constructor(props) {
      super(props);
      this.state = {
          username: '',
          description: '',
          file: null
        };
  
      this.handleChange = this.handleChange.bind(this);
      this.handleFileChange = this.handleFileChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleFileChange(event) {
        console.log(event.target.files[0])
        this.setState({ file:event.target.files[0] });
    }

    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
  
    handleSubmit(event) {
      event.preventDefault();
      var that = this;
      
        this.getBase64(that.state.file, (result) => {
            fetch("http://" + window.location.hostname + ":5000/upload",
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },
                body: JSON.stringify({
                    "username" : that.state.username,
                    "filename" : that.state.file.name,
                    "description" : that.state.description,
                    "filebase64" : result
                })
            })
            .then(function(res){ return res.json(); })
            .then(function(data){ alert( data.msg ) })
        });
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <label>
            Username:
            <input type="text" name="username" value={this.state.username} onChange={this.handleChange} />
          </label>
          <label>
            Description:
            <input type="text" name="description" value={this.state.description} onChange={this.handleChange} />
          </label>
          <br></br><br></br>
          <label>
            File:
            <input type="file" name="file" onChange={this.handleFileChange} />
          </label>
          <br></br><br></br>
          <input type="submit" value="Submit" />
        </form>
      );
    }
  }